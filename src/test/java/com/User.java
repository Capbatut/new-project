package com;

import java.util.Map;

public class User {
    public long id;
    public String username;
    public Map<String, Address> addresses;

    public void setId(long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAddresses(Map<String, Address> addresses) {
        this.addresses = addresses;
    }

    public String getUsername() {
        return username;
    }

    public Map<String, Address> getAddresses() {
        return addresses;
    }

    public long getId() {
        return id;
    }
}
