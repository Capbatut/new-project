package com;

import java.time.LocalDate;
import java.util.List;

public class Order {
    public long id;
    public LocalDate dateTime;
    public User user;
    public List<Item> items;

    public void setId(long id) {
        this.id = id;
    }

    public void setDateTime(LocalDate dateTime) {
        this.dateTime = dateTime;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public long getId() {
        return id;
    }

    public LocalDate getDateTime() {
        return dateTime;
    }

    public User getUser() {
        return user;
    }

    public List<Item> getItems() {
        return items;
    }
}
