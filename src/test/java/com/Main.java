package com;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public HashMap<Long, Order> createOrder() {
        //Create address for user
        Address address1 = new Address();
        address1.setCountry("Moldova");
        address1.setCity("Chisinau");
        address1.setStreet("Kiev 56");

        Address address2 = new Address();
        address2.setCountry("Moldova");
        address2.setCity("Straseni");
        address2.setStreet("KABCD 34");

        HashMap<String, Address> addressMap1 = new HashMap<>() {
        };
        addressMap1.put("Home", address1);
        HashMap<String, Address> addressMap2 = new HashMap<>() {
        };
        addressMap2.put("Office", address2);

        // Create users
        User user = new User();
        user.setId(1);
        user.setUsername("Joe");
        user.setAddresses(addressMap1);

        User user2 = new User();
        user2.setId(2);
        user2.setUsername("Claire");
        user2.setAddresses(addressMap2);

        //Create items
        List<Item> items = new ArrayList<>();
        Item item1 = new Item();
        item1.setId(1);
        item1.setTitle("Item1");
        item1.setType(Type.clothes);
        items.add(item1);

        Item item2 = new Item();
        item2.setId(1);
        item2.setTitle("Item2");
        item2.setType(Type.gadget);
        items.add(item2);

        List<Item> items2 = new ArrayList<>();
        Item item3 = new Item();
        item3.setId(1);
        item3.setTitle("Item3");
        item3.setType(Type.toy);
        items2.add(item3);

//---
        Order myFirstOrder = new Order();
        myFirstOrder.setId(1);
        myFirstOrder.setDateTime(LocalDate.now());
        myFirstOrder.setUser(user);
        myFirstOrder.setItems(items);

        Order mySecondOrder = new Order();
        mySecondOrder.setId(2);
        mySecondOrder.setDateTime(LocalDate.now());
        mySecondOrder.setUser(user2);
        mySecondOrder.setItems(items2);

        //hash map of orders
        HashMap<Long, Order> orders = new HashMap<>();
        orders.put(myFirstOrder.getId(), myFirstOrder);
        orders.put(mySecondOrder.getId(), mySecondOrder);
        return orders;
    }

      /*  Map<Long, Order> result = returnSpecificOrder(orders);
        for (Map.Entry<Long, Order> entry : result.entrySet()) {
            System.out.println("Id - " + entry.getValue().getId() + "\nDate - " + entry.getValue().getDateTime() +
                    "\nUser - " + entry.getValue().getUser().getUsername() + "\nItems - ");
            for (Item itemIter : entry.getValue().getItems()) {
                System.out.println(itemIter.getTitle() + " " + itemIter.getType());
            }
        }*/



    //method that will return only Orders containing item.type = "Clothes"
    public static Map<Long, Order> returnSpecificOrder(HashMap<Long, Order> orders) {
        return orders.entrySet()
                .stream()
                .filter(o -> o.getValue().getItems().stream().anyMatch(item -> item.getType() == Type.clothes))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    //method that will return list of users who's City is Chisinau
    public static List<String> filterUsersByCity(HashMap<Long, Order> orders) {
        List<String> map = new ArrayList<>();
        for (Map.Entry<Long, Order> o : orders.entrySet()) {
            if (o.getValue().getUser().addresses.containsValue("Chisinau")) {
                map.add(o.getValue().getUser().toString());
            }else{
                System.out.println("This user didn't have address in Chisinau ");
            }
        }
        return map;

    }
}
