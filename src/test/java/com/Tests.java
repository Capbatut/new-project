package com;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

public class Tests extends Main {
    @Test
    @DisplayName("Test that verify if Order have item type - clothes - Positive ")
    public void verifyOrderWithClothesType() {
        HashMap<Long, Order> orders = createOrder();
        Map<Long, Order> specificOrder = returnSpecificOrder(orders);
        if (specificOrder.isEmpty()) {
            System.out.println("Test failed - Didn't find orders with item type clothes");
        } else {
            for (Map.Entry<Long, Order> order : specificOrder.entrySet()) {
                Item item = order.getValue().getItems().get(Math.toIntExact(order.getKey() - 1));
                assertEquals(format("Order [%s] has not item type 'clothes'", order.getKey()),
                        Type.clothes,
                        item.getType());
            }
        }
    }

}
