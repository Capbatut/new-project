package com;

public class Address {
    public String country;
    public String city;
    public String street;

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() { return city; }

    public String getFullAddress() {
        return country + city + street;
    }
}
